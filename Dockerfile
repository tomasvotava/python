ARG BASE_IMAGE
FROM ${BASE_IMAGE}

RUN apt-get update && apt-get -y install curl

RUN curl -sSL https://install.python-poetry.org | python -

ENV PATH=/root/.local/bin:$PATH
