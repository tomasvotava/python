#!/usr/bin/env bash

TAGS=(3.10-slim 3.9-slim 3.8-slim 3.10 3.9 3.8)

for i in ${!TAGS[@]}; do
    TAG="${TAGS[$i]}"
    echo "Building from python:${TAG}"
    docker build \
        --build-arg=BASE_IMAGE="python:${TAG}" \
        -t "registry.gitlab.com/tomasvotava/python:${TAG}" \
        .
    docker push "registry.gitlab.com/tomasvotava/python:${TAG}"
done
